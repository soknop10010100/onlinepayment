package com.xssy.takeout.client.md5

import android.content.Context
import android.text.TextUtils
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class MchV1GateWay(
    private val content: Context,
    private val clientKey: String? = null,
) {

    private fun makeSign(body: JsonObject): String {
        val keys: List<String> = getKSort(body)
        val stringBuilder = StringBuilder()
        for (key in keys) {
            val elm = body[key]
            if (key == "sign" || elm.isJsonArray || elm.isJsonNull) {
                continue
            }
            if (elm.isJsonObject) {
                continue
            }
            if (elm.isJsonPrimitive) {
                val jsonPrimitive = elm.asJsonPrimitive
                if (jsonPrimitive.isBoolean && !jsonPrimitive.asBoolean) {
                    continue
                }
                if (jsonPrimitive.isNumber && jsonPrimitive.asNumber.toDouble() == 0.0) {
                    continue
                }
                if (jsonPrimitive.isString && TextUtils.isEmpty(jsonPrimitive.asString)) {
                    continue
                }
            }
            stringBuilder.append(key).append("=").append(elm.asJsonPrimitive)
            val idx = keys.indexOf(key)
            val isAppendAmp = idx != keys.size - 1
            if (isAppendAmp) {
                stringBuilder.append("&")
            }
        }

        stringBuilder.append("&key=").append(clientKey)
        //Todo fix string
        val data = stringBuilder.toString().replace("[\"]".toRegex(), "")
        //Todo md5
        return md5(data)
    }

    private fun md5(s: String): String {
        val MD5 = "MD5"
        try {
            // Create MD5 Hash
            val digest = MessageDigest.getInstance(MD5)
            digest.update(s.toByteArray())
            val messageDigest = digest.digest()

            // Create Hex String
            val hexString = java.lang.StringBuilder()
            for (aMessageDigest in messageDigest) {
                val h =
                    java.lang.StringBuilder(Integer.toHexString(0xFF and aMessageDigest.toInt()))
                while (h.length < 2) h.insert(0, "0")
                hexString.append(h)
            }
            return hexString.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return ""
    }

    private fun getKSort(jsonObject: JsonObject): List<String> {
        val keySort: MutableList<String> = ArrayList()

        for (entry: Map.Entry<String, JsonElement> in jsonObject.entrySet()) {
            keySort.add(entry.key)
        }

        keySort.sortWith { obj: String, anotherString: String? ->
            obj.compareTo(
                anotherString!!
            )
        }
        return keySort
    }

    fun createGateWayService(): JsonObject {
        val body: JsonObject =
            MchV1ParamBuilder.createGateWay()
        val sign = makeSign(body)
        body.addProperty("sign", sign)
        return body
    }

    fun createPreOrderGateWayService(bic: String, onlyDeepLink: Int): JsonObject {
        val body: JsonObject =
            MchV1ParamBuilder.createPreOrderGateWay(bic, onlyDeepLink)
        val sign = makeSign(body)
        body.addProperty("sign", sign)
        return body
    }


}

