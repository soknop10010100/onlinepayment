package com.xssy.takeout.client.model

import com.google.gson.annotations.SerializedName

data class WECHAT(
    @SerializedName("inAppParam" ) var inAppParam : String? = null,
    @SerializedName("expires_in" ) var expiresIn  : Int?    = null,
    @SerializedName("time"       ) var time       : Int?    = null
)
