package com.xssy.takeout.client.model

import com.google.gson.annotations.SerializedName

data class PaymentMethod(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("title") var title: String? = null,
    @SerializedName("img_url") var imgUrl: String? = null,
    @SerializedName("bic") var bic: String? = null,
    @SerializedName("storelink") var storelink: Storelink? = Storelink()
)
