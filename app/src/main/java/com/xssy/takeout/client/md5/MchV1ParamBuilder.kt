package com.xssy.takeout.client.md5

import com.google.gson.JsonObject
import com.xssy.takeout.client.util.Config
import java.util.*

object MchV1ParamBuilder {

    fun createGateWay(): JsonObject {
        val jsonObject = JsonObject()
        jsonObject.addProperty("sign_type", "MD5")
        jsonObject.addProperty("service", "webpay.acquire.getpaymentmethods")
        return jsonObject
    }

    fun createPreOrderGateWay(bic: String, onlyDeepLink: Int): JsonObject {
        val jsonObject = JsonObject()
        jsonObject.addProperty("sign_type", "MD5")
        jsonObject.addProperty("service", "webpay.acquire.nativePay")
        jsonObject.addProperty("seller_code", "CU2211-27962254758512870")
        jsonObject.addProperty(
            "out_trade_no", UUID.randomUUID().toString()
        )
        jsonObject.addProperty("body", "Test-0001")
        jsonObject.addProperty("total_amount", 5)
        jsonObject.addProperty("currency", "USD")
        jsonObject.addProperty("login_type", "GENERAL")
        jsonObject.addProperty("sign_type", "MD5")
        jsonObject.addProperty("service_code", bic)
        //skip this field
        if (onlyDeepLink == Config.support_only_deeplink) {
            jsonObject.addProperty("only_deeplink", 1)
        }
        return jsonObject
    }


}