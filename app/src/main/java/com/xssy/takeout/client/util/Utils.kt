package com.xssy.takeout.client.util

import android.content.Context
import android.content.pm.PackageManager
import android.widget.ImageView
import cn.pedant.SweetAlert.SweetAlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.gson.GsonBuilder
import com.xssy.takeout.client.PaymentDemoApi
import com.xssy.takeout.client.header.MyServiceInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Utils {

    private fun okHttpClient(context: Context): OkHttpClient {
        val timeOut: Long = 120
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(MyServiceInterceptor(context))
        httpClient.addNetworkInterceptor(com.xssy.takeout.client.header.LogDemoJson.LogDataGson())
        httpClient.connectTimeout(timeOut, TimeUnit.SECONDS)
        httpClient.writeTimeout(timeOut, TimeUnit.SECONDS)
        httpClient.readTimeout(timeOut, TimeUnit.SECONDS)
        return httpClient.build()
    }

    fun kessPaymentService(context: Context): PaymentDemoApi {
        val gson =
            GsonBuilder()
                .setLenient()
                .create()
        return Retrofit.Builder()
            .baseUrl("https://devwebpayment.kesspay.io/")
            .client(okHttpClient(context))
            .addConverterFactory(GsonConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(PaymentDemoApi::class.java)
    }

    fun setSharePreference(context: Context, PREF_NAME: String, VALUE: String) {
        val prefs =
            context.getSharedPreferences(Config.KESSPaymentDemoPREF_NAME, Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.putString(PREF_NAME, VALUE)
        editor.apply()
    }

    fun getSharePreference(context: Context, PREF_NAME: String): String {
        val prefs =
            context.getSharedPreferences(Config.KESSPaymentDemoPREF_NAME, Context.MODE_PRIVATE)
        return prefs.getString(PREF_NAME, "") ?: ""
    }

    fun showError(context: Context, text: String?) {
        var text = text
        try {
            text = text?.replace("\r\n".toRegex(), "<br />") ?: ""
            val alertDialog: SweetAlertDialog =
                SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                    /* .setTitleText(title ?: "---")*/
                    .setContentText(String.format("%s", text))
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        } catch (ex: RuntimeException) {
            ex.printStackTrace()
        }
    }

    fun showSuccess(
        context: Context?,
        text: String?,
        onSweetClickListener: SweetAlertDialog.OnSweetClickListener?,
    ) {
        var text = text
        try {
            text = text?.replace("\r\n".toRegex(), "<br />") ?: ""
            val alertDialog: SweetAlertDialog =
                SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                    .setContentText(text)
                    .setConfirmClickListener(onSweetClickListener)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun loadImage(
        context: Context,
        resource: Any,
        imageView: ImageView,
    ) {
        Glide.with(context)
            .load(resource)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(imageView)
    }


    fun hasAppInstalled(packageName: String, packageManager: PackageManager): Boolean? {
        return try {
            packageManager.getPackageInfo(packageName, 0)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }
}