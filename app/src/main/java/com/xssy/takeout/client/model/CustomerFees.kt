package com.xssy.takeout.client.model

import com.google.gson.annotations.SerializedName

data class CustomerFees(
    @SerializedName("ALIPAY") var ALIPAY: ALIPAY? = ALIPAY()
)