package com.xssy.takeout.client

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Toast
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.xssy.takeout.client.util.Utils
import com.xssy.takeout.client.md5.MchV1GateWay
import com.xssy.takeout.client.model.ApiResWraper
import com.xssy.takeout.client.model.AuthToken
import com.xssy.takeout.client.model.PaymentMethod
import com.xssy.takeout.client.model.PaymentDemoSingleTon
import com.xssy.takeout.client.ui.PaymentListsActivity
import com.xssy.takeout.client.util.Config
import com.xssy.takeout.client.wxapi.AlertDialogActivity
import com.xssy.takeout.client.wxapi.MyBroadcastReceiver
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

const val clientKey = "9bQgShZgl8onuWEMbOTny4NzMjcmP3cpFKWudW7XmBpjg"
const val grant_type = "password"
const val client_id = "3fc94387-702a-4896-aea3-1d7cb565c1ea"
const val client_secret = "dmj6gcAQPD7Kh7LVNphm69AN04bdjcxsMF4nZA3tVbTcc"
const val username = "cheachea168@gmail.com"
const val password = "w2ivYXZRX4zDnkuBQXUiNwd2rRv6p9wJEkVkHMA0RqrEJ"

class MainActivity : AppCompatActivity() {
    val TAG = "mMainActivity"

    val filter = IntentFilter(Config.WE_CHAT_BR_ID)

    private lateinit var loadingLayout: View
    private lateinit var paymentService: PaymentDemoApi


    private var receiver: MyBroadcastReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        receiver = object : MyBroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {

                p1?.let {
                    val code = it.getIntExtra("error_code", -1)
                    val str = it.getStringExtra("error_string")
                    val trx = it.getStringExtra("transaction")

                    Toast.makeText(
                        this@MainActivity,
                        "code: $code, string: $str, transaction: $trx",
                        Toast.LENGTH_LONG).show()
                }
            }
        }

        loadingLayout = findViewById(R.id.loading_layout)
        paymentService = Utils.kessPaymentService(this)

        //loadAuthToken()
        registerReceiver(receiver, filter)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver)
    }

    private fun loadAuthToken() {

        val requestBody = HashMap<String, Any>()
        requestBody["grant_type"] = grant_type.trim()
        requestBody["client_id"] =client_id.trim()
        requestBody["client_secret"] =client_secret.trim()
        requestBody["username"] =username.trim()
        requestBody["password"] =password.trim()

        loadingLayout.visibility = View.VISIBLE
        paymentService.authToken(requestBody).enqueue(object : Callback<AuthToken> {
            override fun onResponse(
                call: Call<AuthToken>, response: Response<AuthToken>,
            ) {
                loadingLayout.visibility = View.GONE
                if (response.body() != null && response.body()?.accessToken != null) {
                    PaymentDemoSingleTon.newInstance?.accessToken = response.body()?.accessToken
                    doPayment()
                } else {
                    Utils.showError(
                        this@MainActivity, response.errorBody()?.string().toString()?:"Invalid Token."
                    )
                }
            }

            override fun onFailure(call: Call<AuthToken>, t: Throwable) {
                loadingLayout.visibility = View.GONE
                val errorStr = Gson().toJson(t.message)
                Utils.showError(
                    this@MainActivity, errorStr
                )
            }

        })
    }

    fun doPayment() {
        val mchV1GateWay = MchV1GateWay(this, Config.clientKey)
        val requestJson = mchV1GateWay.createGateWayService()

        loadingLayout.visibility = View.VISIBLE
        paymentService.gateway(requestJson)
            .enqueue(object : Callback<ApiResWraper<ArrayList<PaymentMethod>>> {
                override fun onResponse(
                    call: Call<ApiResWraper<ArrayList<PaymentMethod>>>,
                    response: Response<ApiResWraper<ArrayList<PaymentMethod>>>,
                ) {
                    loadingLayout.visibility = View.GONE
                    response.body()?.let {

                        val paymentMethods = it.data
                        val intent = Intent(this@MainActivity, PaymentListsActivity::class.java)
                        intent.putExtra("paymentMethodsKey", Gson().toJson(paymentMethods))
                        startActivity(intent)

                    } ?: Utils.showError(
                        this@MainActivity, "Invalid Please Retry!"
                    )
                }

                override fun onFailure(
                    call: Call<ApiResWraper<ArrayList<PaymentMethod>>>,
                    t: Throwable,
                ) {
                    loadingLayout.visibility = View.GONE
                    val errorStr = Gson().toJson(t.message)
                    Utils.showError(
                        this@MainActivity, errorStr
                    )
                }

            })
    }


    fun actionFetchToken(view: View) {
        loadAuthToken()
    }
}