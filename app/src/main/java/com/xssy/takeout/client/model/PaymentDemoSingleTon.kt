package com.xssy.takeout.client.model

class PaymentDemoSingleTon {
    companion object {
        private var ourInstance: PaymentDemoSingleTon? = null

        @JvmStatic
        val newInstance: PaymentDemoSingleTon?
            get() {
                if (ourInstance == null) {
                    ourInstance = PaymentDemoSingleTon()
                }
                return ourInstance
            }

    }

    var accessToken: String? = ""

}