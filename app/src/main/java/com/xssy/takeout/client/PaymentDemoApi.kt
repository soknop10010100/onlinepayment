package com.xssy.takeout.client

import com.google.gson.JsonObject
import com.xssy.takeout.client.model.ApiResWraper
import com.xssy.takeout.client.model.AuthToken
import com.xssy.takeout.client.model.PaymentMethod
import com.xssy.takeout.client.model.PreOrderInfo
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface PaymentDemoApi {

    @POST("oauth/token")
    fun authToken(@Body map: HashMap<String, Any>): Call<AuthToken>

    @POST("api/mch/v2/gateway")
    fun gateway(@Body body: JsonObject): Call<ApiResWraper<ArrayList<PaymentMethod>>>

    @POST("api/mch/v2/gateway")
    fun preOrderGateway(@Body body: JsonObject): Call<ApiResWraper<PreOrderInfo>>

}