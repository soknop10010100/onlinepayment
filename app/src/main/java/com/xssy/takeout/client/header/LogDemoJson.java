package com.xssy.takeout.client.header;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.xssy.takeout.client.util.Config;
import com.xssy.takeout.client.util.AppLOGG;

public class LogDemoJson {
    public static CustomHttpLogging LogDataGson() {
        CustomHttpLogging customHttpLogging = new CustomHttpLogging(message -> {

            if (!message.startsWith("{")) {
                AppLOGG.INSTANCE.d(Config.pretty_gson, message);
                return;
            }
            try {
                /* new JsonParser().parse(message) deprecated */
                JsonElement jsonElement = new JsonParser().parse(message);// JsonParser.parseString(message);
                String prettyPrintJson = new GsonBuilder().setPrettyPrinting().create().toJson(jsonElement);
                AppLOGG.INSTANCE.d(Config.pretty_gson, prettyPrintJson);
            } catch (JsonSyntaxException m) {
                AppLOGG.INSTANCE.d(Config.pretty_gson, message);
            }
        });
        customHttpLogging.setLevel(CustomHttpLogging.Level.BODY);
        return customHttpLogging;
    }
}
