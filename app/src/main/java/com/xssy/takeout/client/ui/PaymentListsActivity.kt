package com.xssy.takeout.client.ui

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alipay.sdk.app.PayTask
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import com.xssy.takeout.client.PaymentDemoApi

import com.xssy.takeout.client.R
import com.xssy.takeout.client.md5.MchV1GateWay
import com.xssy.takeout.client.model.*
import com.xssy.takeout.client.util.Config
import com.xssy.takeout.client.util.PaymentListAdapter
import com.xssy.takeout.client.util.Utils
import com.stripe.android.model.WeChat
import com.tencent.mm.opensdk.modelpay.PayReq
import com.tencent.mm.opensdk.openapi.WXAPIFactory
import com.xssy.takeout.client.PayResult
import org.apache.commons.text.StringEscapeUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PaymentListsActivity : AppCompatActivity() {

    private lateinit var paymentMethodRecyclerView: RecyclerView
    private lateinit var paymentMethodAdapter: PaymentListAdapter
    private lateinit var loadingLayout: View
    private lateinit var paymentService: PaymentDemoApi

    private var bic: String = ""
    private val appID = Config.APP_ID

    companion object {
        const val TAG = "PaymentListsActivity"
        private const val SDK_PAY_FLAG = 1

        const val acledaID = "com.domain.acledabankqr"
        const val abaID = "com.paygo24.ibank"
        const val alipayID = "com.eg.android.AlipayGphone"
        const val wechatID = "com.tencent.mm"
        const val sathapanaID = "kh.com.sathapana.consumer"

        const val visa_master = "VISA_MASTER"
        const val aba = "ABAAKHPP"
        const val acleda = "ACLBKHPP"
        const val wechat = "WECHAT"
        const val alipay = "ALIPAY"
        const val wing = "wing"
        const val sathapana = "SBPLKHPP"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_lists)

        paymentMethodRecyclerView = findViewById(R.id.payment_list_recycler_view)
        loadingLayout = findViewById(R.id.loading_layout)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Payment Lists"

        paymentMethodAdapter = PaymentListAdapter()
        paymentService = Utils.kessPaymentService(this)

        paymentMethodRecyclerView.apply {
            layoutManager =
                LinearLayoutManager(this@PaymentListsActivity, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    this@PaymentListsActivity, DividerItemDecoration.VERTICAL
                )
            )
            adapter = paymentMethodAdapter

        }

        loadPayments()
        doAction()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    private fun loadPayments() {
        val intent = intent
        val typeToken = object : TypeToken<ArrayList<PaymentMethod>>() {}.type
        val paymentMethods: ArrayList<PaymentMethod> =
            Gson().fromJson(intent.getStringExtra("paymentMethodsKey"), typeToken)

        if (paymentMethods.size > 0) {
            paymentMethodAdapter.addItem(paymentMethods)
        }

    }

    private fun doAction() {
        paymentMethodAdapter.selectedRow = { paymentMethod ->
            paymentMethod.bic?.let { bic ->
                this.bic = bic
                val mchV1GateWay = MchV1GateWay(this, Config.clientKey)

                var supportOnlyDeeplink = Config.support_only_deeplink
                if (bic.equals(visa_master, true)) {
                    supportOnlyDeeplink = Config.not_support_only_deeplink
                }

                val requestJson =
                    mchV1GateWay.createPreOrderGateWayService(bic, supportOnlyDeeplink)

                loadingLayout.visibility = View.VISIBLE
                paymentService.preOrderGateway(requestJson)
                    .enqueue(object : Callback<ApiResWraper<PreOrderInfo>> {
                        override fun onResponse(
                            call: Call<ApiResWraper<PreOrderInfo>>,
                            response: Response<ApiResWraper<PreOrderInfo>>,
                        ) {
                            loadingLayout.visibility = View.GONE
                            if (response.isSuccessful) {
                                if (response.body() != null && response.body()!!.data != null) {
                                    val orderInfo = response.body()!!.data
                                    if (orderInfo != null) {
                                        redirectApp(orderInfo)
                                    } else {
                                        Utils.showError(
                                            this@PaymentListsActivity, "data is null"
                                        )
                                    }
                                }
                            } else {
                                Utils.showError(
                                    this@PaymentListsActivity,
                                    response.errorBody()?.string().toString()
                                )
                            }
                        }

                        override fun onFailure(
                            call: Call<ApiResWraper<PreOrderInfo>>, t: Throwable,
                        ) {
                            loadingLayout.visibility = View.GONE
                            val errorStr = Gson().toJson(t.message)
                            Utils.showError(
                                this@PaymentListsActivity, errorStr
                            )
                        }
                    })


            } ?: Utils.showError(
                this, "Field bic is null"
            )

        }
    }

    private fun redirectApp(orderInfo: PreOrderInfo) {

        if (bic.equals(alipay, true)) {
            payViaAlipay(orderInfo)
            return
        }

        if (bic.equals(wechat, true)) {
            payViaWechat(orderInfo)
            return
        }

        if (orderInfo.deeplink == null) {
            Utils.showError(this, "Deeplink is null")
            return
        }

        if (bic.equals(aba, true)) {
            payViaAba(orderInfo)
        } else if (bic.equals(acleda, true)) {
            payViaAcleda(orderInfo)
        } else if (bic.equals(visa_master, true)) {
            val intent = Intent(this, WebPayActivity::class.java)
            intent.putExtra("url_key", orderInfo.deeplink)
            startActivity(intent)
        } else if (bic.equals(wing, true)) {
            val intent = Intent(this, WebPayActivity::class.java)
            intent.putExtra("url_key", orderInfo.deeplink)
            startActivity(intent)
        }else if (bic.equals(sathapana, true)) {
            paySathapana(orderInfo)
        } else {
            val intent = Intent(this, WebPayActivity::class.java)
            intent.putExtra("url_key", orderInfo.deeplink)
            startActivity(intent)
        }
    }
    //pay with Sathapana
    private fun paySathapana(orderInfo: PreOrderInfo) {
        if (Utils.hasAppInstalled(sathapanaID, packageManager) == true) {
            openDeepLink(Uri.parse(orderInfo.deeplink))
        } else {
            gotoPlayStore(sathapanaID)
        }
    }

    //pay with wechat
    private fun payViaWechat(prOrderInfo: PreOrderInfo) {

        if (Utils.hasAppInstalled(wechatID, packageManager) == true) {
            val wxAPI = WXAPIFactory.createWXAPI(this, appID, true)
            wxAPI.registerApp(appID)
            prOrderInfo.orderInfo?.meta?.generatedInappParams?.WECHAT?.let { wechat ->
                val inAppParamStr = wechat.inAppParam
                try {
                    val inAppParam = Gson().fromJson(
                        StringEscapeUtils.unescapeJson(inAppParamStr),
                        InAppParamModel::class.java
                    )
                    val weChatPay = WeChat(
                        packageValue = inAppParam.packageStr,
                        appId = appID,
                        nonce = inAppParam.nonceStr,
                        partnerId = inAppParam.partnerId,
                        prepayId = inAppParam.prepayId,
                        timestamp = inAppParam.timestamp,
                        sign = inAppParam.sign
                    )

                    val payReq = PayReq()
                    payReq.appId = appID
                    payReq.packageValue = weChatPay.packageValue
                    payReq.nonceStr = weChatPay.nonce
                    payReq.partnerId = weChatPay.partnerId
                    payReq.prepayId = weChatPay.prepayId
                    payReq.timeStamp = weChatPay.timestamp
                    payReq.sign = weChatPay.sign
                    val check = wxAPI.sendReq(payReq)
                    Toast.makeText(this, "Request sent: $check", Toast.LENGTH_LONG).show()

                } catch (_: JsonSyntaxException) {
                }
            }
        } else {
            gotoPlayStore(wechatID)
        }

    }

    //pay with acleda
    private fun payViaAcleda(orderInfo: PreOrderInfo) {
        if (Utils.hasAppInstalled(acledaID, packageManager) == true) {
            openDeepLink(Uri.parse(orderInfo.deeplink))
        } else {
            gotoPlayStore(acledaID)
        }
    }

    //pay with aba
    private fun payViaAba(orderInfo: PreOrderInfo) {
        if (Utils.hasAppInstalled(abaID, packageManager) == true) {
            openDeepLink(Uri.parse(orderInfo.deeplink))
        } else {
            gotoPlayStore(abaID)
        }
    }

    //pay with alipay
    private lateinit var payTask: PayTask
    private fun payViaAlipay(orderInfo: PreOrderInfo) {
        Gson().toJson(orderInfo.inAppParam)
        if (Utils.hasAppInstalled(alipayID, packageManager) == true) {
            val payRunnable = Runnable {
                payTask = PayTask(this)
                val result = payTask.payV2(orderInfo.inAppParam, true)
                val msg = Message()
                msg.what = SDK_PAY_FLAG
                msg.obj = result
                mAlipayHandler.sendMessage(msg)
            }
            // 必须异步调用
            val payThread = Thread(payRunnable)
            payThread.start()

        } else {
            gotoPlayStore(alipayID)
        }
    }

    private fun openDeepLink(uri: Uri) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = uri
        startActivity(intent)
    }

    private fun gotoPlayStore(applicationId: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(
            String.format(
                "%s%s",
                "https://play.google.com/store/apps/details?id=",
                applicationId
            )
        )
        startActivity(intent)
    }

    private val mAlipayHandler: Handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                SDK_PAY_FLAG -> {
                    val payResult =
                        PayResult(msg.obj as Map<String, String>)
                    val resultInfo = payResult.result // 同步返回需要验证的信息
                    val resultStatus = payResult.resultStatus
                    if (TextUtils.equals(resultStatus, "9000")) {
                        Utils.showSuccess(
                            this@PaymentListsActivity, "Payment success: ${resultInfo}"
                        ) {}
                    } else {
                        Utils.showError(
                            this@PaymentListsActivity,
                            "Payment failed: ${resultInfo}"
                        )
                    }
                }
            }
        }
    }

}