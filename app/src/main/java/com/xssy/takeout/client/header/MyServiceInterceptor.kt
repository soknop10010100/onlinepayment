package com.xssy.takeout.client.header

import android.content.Context
import com.xssy.takeout.client.model.PaymentDemoSingleTon
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class MyServiceInterceptor(private val context: Context) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val builder: Request.Builder = request.newBuilder()
        builder.addHeader("Accept", "*/*")
        //builder.addHeader("x-device-type", "mobile")
        builder.addHeader("Content-Type", "application/json")
        builder.addHeader("Accept-Encoding", "Accept-Encoding")
        builder.addHeader("Content-Encoding", "gzip")

        if (PaymentDemoSingleTon.newInstance?.accessToken != null) {
            val accessToken = PaymentDemoSingleTon.newInstance?.accessToken
            builder.addHeader("Authorization", "Bearer $accessToken")
        }

        val requestBuilder = builder.build()
        return chain.proceed(requestBuilder)
    }

}