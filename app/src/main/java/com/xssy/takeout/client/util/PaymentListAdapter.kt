package com.xssy.takeout.client.util

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.xssy.takeout.client.R
import com.xssy.takeout.client.model.PaymentMethod
import java.util.ArrayList

class PaymentListAdapter : RecyclerView.Adapter<PaymentListAdapter.PaymentListHolder>() {
    private var paymentMethods = ArrayList<PaymentMethod>()
    lateinit var selectedRow: (PaymentMethod) -> Unit

    fun addItem(paymentMethods: ArrayList<PaymentMethod>) {
        this.paymentMethods.clear()
        this.paymentMethods.addAll(paymentMethods)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentListHolder {
        return PaymentListHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.payment_method_row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: PaymentListHolder, position: Int) {
        val model = paymentMethods[position]
        holder.bind(model)

        holder.itemView.setOnClickListener {
            selectedRow.invoke(model)
        }
    }

    override fun getItemCount(): Int {
        return paymentMethods.size
    }

    class PaymentListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val logoImg: ImageView = itemView.findViewById(R.id.logo_img)
        private val logoNameTv: TextView = itemView.findViewById(R.id.logo_name_tv)

        fun bind(paymentMethod: PaymentMethod) {
            Log.d("paymentMethod", "bind: ")
            Utils.loadImage(itemView.context, paymentMethod.imgUrl ?: "", logoImg)
            logoNameTv.text = String.format("%s", paymentMethod.title)

        }

    }
}
