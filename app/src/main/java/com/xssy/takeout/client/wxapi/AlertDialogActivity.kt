package com.xssy.takeout.client.wxapi

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog

class AlertDialogActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        window.setFlags(1024, 1024)
        supportActionBar?.let {
            it.hide()
            it.setDisplayShowTitleEnabled(false)
        }
        super.onCreate(savedInstanceState)
        val alertDialog: SweetAlertDialog =
            SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setContentText("text")
                .setConfirmClickListener {
                    finish()
                }
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.show()

        alertDialog.setOnDismissListener {
            finish()
        }
    }
}