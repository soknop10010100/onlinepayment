package com.xssy.takeout.client.model

import com.google.gson.annotations.SerializedName

data class Setting(
    @SerializedName("template") var template: String? = null
)
