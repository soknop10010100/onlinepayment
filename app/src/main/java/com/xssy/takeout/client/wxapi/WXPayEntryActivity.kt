package com.xssy.takeout.client.wxapi

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.tencent.mm.opensdk.modelbase.BaseReq
import com.tencent.mm.opensdk.modelbase.BaseResp
import com.tencent.mm.opensdk.openapi.IWXAPI
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler
import com.tencent.mm.opensdk.openapi.WXAPIFactory
import com.xssy.takeout.client.R
import com.xssy.takeout.client.util.Config

class WXPayEntryActivity : AppCompatActivity(), IWXAPIEventHandler {

    private lateinit var api: IWXAPI
    private lateinit var text3: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wxpay_entry)
        text3 = findViewById(R.id.textView3)
        api = WXAPIFactory.createWXAPI(this, Config.APP_ID)
        api.handleIntent(intent, this)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        api.handleIntent(intent, this)
    }

    private val TAG = "log_dataWXPayEntryActivity"
    override fun onReq(p0: BaseReq?) {
        Log.d(TAG, "onReq: ${p0?.transaction}")
    }

    override fun onResp(baseResp: BaseResp?) {
        Toast.makeText(this, "Received callback", Toast.LENGTH_LONG).show()
        Log.e(TAG, "onResp: ...")
        baseResp?.let {
            val res = "onResp: " + baseResp.errStr.toString() + " " + baseResp.errCode
            text3.text = res
            Log.e(TAG, res)
            val intent = Intent(Config.WE_CHAT_BR_ID)
            intent.putExtra("error_code", baseResp.errCode)
            intent.putExtra("error_string", baseResp.errStr)
            intent.putExtra("transaction", baseResp.transaction)
            sendBroadcast(intent)
        }
        finish()
    }
}