package com.xssy.takeout.client.model

import com.google.gson.annotations.SerializedName

data class PreOrderInfo(
    @SerializedName("qrcode") var qrcode: String? = null,
    @SerializedName("deeplink") var deeplink: String? = null,
    @SerializedName("expires_in") var expiresIn: Int? = null,
    @SerializedName("brand_logo") var brandLogo: String? = null,
    @SerializedName("app_name") var appName: String? = null,
    @SerializedName("service_code") var serviceCode: String? = null,
    @SerializedName("order_info") var orderInfo: OrderInfo? = OrderInfo(),
    @SerializedName("inAppParam") var inAppParam: String? = null
)
