package com.xssy.takeout.client.model

import com.google.gson.annotations.SerializedName

data class GeneratedInappParams(
    @SerializedName("WECHAT") var WECHAT: WECHAT? = WECHAT()
)
