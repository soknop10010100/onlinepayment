package com.xssy.takeout.client.ui


import android.graphics.Bitmap
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.xssy.takeout.client.R
import com.xssy.takeout.client.util.Utils


class WebPayActivity : AppCompatActivity() {

    private lateinit var webPayWebView: WebView
    private lateinit var loadingLayout: View

    companion object {
        const val JAVASCRIPT_OBJ = "javascript_obj"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_pay)

        loadingLayout = findViewById(R.id.loading_layout)
        webPayWebView = findViewById(R.id.web_pay_)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "WebPay"

        webPayWebView.settings.apply {
            javaScriptEnabled = true
            useWideViewPort = true
            cacheMode = WebSettings.LOAD_NO_CACHE
        }
        webPayWebView.webViewClient = webViewClient
        webPayWebView.addJavascriptInterface(JavaScriptInterface(), JAVASCRIPT_OBJ)

        val url = intent.getStringExtra("url_key") ?: ""
        webPayWebView.loadUrl(url)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    private val webViewClient = object : WebViewClient() {

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            loadingLayout.visibility = View.VISIBLE
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            loadingLayout.visibility = View.GONE

            injectJavaScriptFunction()
        }

        override fun onReceivedError(
            view: WebView?, request: WebResourceRequest?, error: WebResourceError?,
        ) {
            super.onReceivedError(view, request, error)
            loadingLayout.visibility = View.GONE
        }

    }

    private fun injectJavaScriptFunction() {
        webPayWebView.loadUrl("javascript: window.WebPayJSBride.invoke = function(data) { $JAVASCRIPT_OBJ.onPaymentSuccess(data) }")
    }

    inner class JavaScriptInterface {
        @JavascriptInterface
        fun onPaymentSuccess(data: String) {
            if (!TextUtils.isEmpty(data) && data == "onPaymentSuccess") {
                Utils.showSuccess(this@WebPayActivity, "Payment Successful") {
                    it.dismiss()
                    finish()
                }
            }
        }
    }

}