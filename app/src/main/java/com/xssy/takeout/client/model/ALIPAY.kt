package com.xssy.takeout.client.model

import com.google.gson.annotations.SerializedName

data class ALIPAY(
    @SerializedName("fee") var fee: Int? = null,
    @SerializedName("currency") var currency: String? = null
)
