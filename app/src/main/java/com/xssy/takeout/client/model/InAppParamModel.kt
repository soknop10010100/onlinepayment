package com.xssy.takeout.client.model

import com.google.gson.annotations.SerializedName

data class InAppParamModel(
    @SerializedName("appid") var appid: String? = null,
    @SerializedName("noncestr") var nonceStr: String? = null,
    @SerializedName("package") var packageStr: String? = null,
    @SerializedName("partnerid") var partnerId: String? = null,
    @SerializedName("prepayid") var prepayId: String? = null,
    @SerializedName("timestamp") var timestamp: String? = null,
    @SerializedName("sign") var sign: String? = null
)
