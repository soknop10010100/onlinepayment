package com.xssy.takeout.client.model

import com.google.gson.annotations.SerializedName

data class OrderInfo(
    @SerializedName("token") var token: String? = null,
    @SerializedName("out_trade_no") var outTradeNo: String? = null,
    @SerializedName("transaction_id") var transactionId: String? = null,
    @SerializedName("body") var body: String? = null,
    @SerializedName("total_amount") var totalAmount: Int? = null,
    @SerializedName("currency") var currency: String? = null,
    @SerializedName("version") var version: String? = null,
    @SerializedName("meta") var meta: Meta? = Meta(),
    @SerializedName("status") var status: String? = null,
    @SerializedName("paid_at") var paidAt: String? = null,
    @SerializedName("settled_at") var settledAt: String? = null,
    @SerializedName("settlement_date") var settlementDate: String? = null,
    @SerializedName("expired_at") var expiredAt: String? = null,
    @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("detail") var detail: ArrayList<String> = arrayListOf(),
    @SerializedName("seller") var seller: Seller? = Seller(),
    @SerializedName("payment_detail") var paymentDetail: String? = null,
    @SerializedName("error_logs") var errorLogs: ArrayList<String> = arrayListOf(),
    // @SerializedName("card_info") var cardInfo: ArrayList<String> = arrayListOf()
)
